<?php
/**
 * Plugin name: Tridaz Plugin Configuration Exporter.
 * Description: Use this plugin to export all your plugins to a composer.json file.
 * Version: 1.0.0
 * Plugin URI: https://www.tridaz.eu
 * Author: Tridaz
 */

/**
 * Autoloads files when requested
 *
 * @since  1.0.0
 *
 * @param  string $class_name Name of the class being requested
 */
function tridaz_autoload( $class_name ) {

	/**
	 * If the class being requested does not start with our prefix,
	 * we know it's not one in our project
	 */
	if ( 0 !== strpos( $class_name, 'Tridaz_' ) ) {
		return;
	}

	// Make classname lowercase, and replace underscores with hyphens
	$file_name = str_replace( '_', '-', strtolower( $class_name ) );

	// Compile our path from the current location (with "class-" prefix)
	$file = dirname( __FILE__ ) . '/inc/class-' . $file_name . '.php';

	// If a file is found
	if ( file_exists( $file ) ) {
		// Then load it up!
		require( $file );
	}
}

spl_autoload_register( "tridaz_autoload" );

// Start plugin.
$plugin_config = new Tridaz_Plugin_Config();